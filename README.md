# astro_raspberry_build
[![build status](https://gitlab.com:gnthibault/astro_control_box/badges/NewBuild/build.svg)](https://gitlab.com:gnthibault/astro_control_box/commits/NewBuild)

## Cloning the repository and its git submodules
The overall software embedded in the computer powering the telescope shelter is composed of multiple different softwares, that are included as submodules of this repository. This requires some attention when cloning this project.
There are two options:
* The simplest option is:

    ```bash
    git clone --recursive git@gitlab.com:gnthibault/astro_control_box.git
    ```

    This will download the submodule up to the version that is used in this project. To update to the latest commit of the submodule itself:

    ```bash
    git submodule update --remote
    ```

* One could also directly download the submodule from the source:

    ```bash
    git clone git@gitlab.com:gnthibault/astro_control_box.git
    cd ./plugins/PLUGIN_1
    git submodule init
    git submodule update
    ```

### Dependencies

If you want to build this project, you might have to install some prerequisites (example for ubuntu):

  ```bash
  sudo apt-get update
  sudo bash ./install_prerequisite.sh
  ```


## (optional) installing gitlab runners
Check out [gitlab documentation](https://docs.gitlab.com/runner/install/linux-manually.html) or [here](https://docs.gitlab.com/runner/install/linux-repository.html) to install the runners on you CI server
A full list of supported architectures can be found [here](https://gitlab-runner-downloads.s3.amazonaws.com/latest/index.html), but we provide samples for two common samples:
* x86
  ```bash
  curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb"
  sudo dpkg -i gitlab-runner_amd64.deb
  ```

* arm64:
  ```bash
  curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_arm64.deb"
  sudo dpkg -i gitlab-runner_arm64.deb
  ```

Ok, now, given the non standard workflow (building arm64 image from x86), it will be needed to do some changes in order for gitlab runners to be able to run the sudo commands.
Documentation from this part has been taken from [this SO post](from https://stackoverflow.com/questions/19383887/how-to-use-sudo-in-build-script-for-gitlab-ci)

You can grant sudo permissions to the gitlab-runner user as this is who is executing the build script.
  ```bash
  sudo usermod -a -G sudo gitlab-runner
  ```

You now have to remove the password restriction for sudo for the gitlab-runner user:

*Start the sudo editor with
  ```bash
  sudo visudo
  ```

*Now add the following to the bottom of the file
  ```bash
  gitlab-runner ALL=(ALL) NOPASSWD: ALL
  ```

*You may also prefer specifying which command the sudo user can use witout pwd
  ```bash
  gitlab-runner ALL=(ALL) NOPASSWD: /usr/bin/npm
  ```

### Registering runners against project instance
Checkout the actual config for your instance, url looks like this for this project: https://gitlab.com/gnthibault/astro_control_box/-/settings/ci_cd
You can find it inside Settings - CI/CD - then expand **Runners** tab
You will find the instance url: https://gitlab.com/
And the registration token:
XXXXXXXXXXX

then run
  ```bash
  sudo gitlab-runner register
  ```
then you will be prompted for instructions, a typical working configuration is the following:
* Enter the GitLab instance URL (take it from  https://gitlab.com/gnthibault/astro_control_box/-/settings/ci_cd)
* Enter the registration token (take it from  https://gitlab.com/gnthibault/astro_control_box/-/settings/ci_cd)
* Enter a description for the runner: office_server_room_raspberry4
* Enter tags for the runner (comma-separated): build,linux,test,arm64
* Enter an executor: select shell executor, as we target a simple build scenario for now


## How to build the image
Run:

  ```bash
  bash download_image.sh
  sudo bash mount_image.sh
  ```

