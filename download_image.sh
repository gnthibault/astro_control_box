#!/bin/bash

# First, image related variables
img_dl_url="https://cdimage.ubuntu.com/releases/20.04.3/release/ubuntu-20.04.5-preinstalled-server-arm64+raspi.img.xz"
img_sha="44b98acd3fd4379c6b194696520b6aecb2f596b601e43e9b6934c83f0aa61026"
# Script related utilities
image_path=./downloads
image_xz="$image_path/image.img.xz"
image_iso="$image_path/image.img"

download_xz_image() {
  # Consider checking latest ver/sha online, download only if newer
  # https://downloads.raspberrypi.org/raspbian_lite/images/?C=M;O=D
  # For now just delete any prior download zip to force downloading latest version
  if [ -f $image_xz ]; then
    check_xz_sha
    error_code=$?
    if [ $error_code -eq 0 ]; then
      return 0
    else
      echo "Previous file $image_xz corrupted, removing"
      rm -f $image_xz
    fi
  fi

  # Rest of the script simply assumes that we can download the xz
  mkdir -p $image_path
  echo "Downloading image ..."
  # echo "Downloading latest Raspbian lite image"
  # curl often gave "error 18 - transfer closed with outstanding read data remaining"
  # wget -O $image_zip $img_dl_url
  wget -O $image_xz $img_dl_url

  check_xz_sha
  error_code=$?
  if [ $error_code -ne 0 ]; then
    echo "Download failed" ; exit -1;
  fi
  return 0
}

check_xz_sha() {
  if ! echo "$img_sha  $image_xz" | shasum -a 256 --check -; then
    echo "Checksum failed for file $image_xz" >&2
    return 1
  else
    echo "Checksum is valid for file $image_xz, proceeding"
    return 0
  fi
}

uncompress_xz() {
  # Actually, we will always start from fresh iso
  rm -rf $image_iso
  #if [ ! -f $image_iso ]; then
  echo "Extracting image ${image_xz}"
  #unzip $image_xz > $image_iso
  xz --keep --decompress --threads=4 --stdout $image_xz > $image_iso
  if [ $? -ne 0 ]; then
    echo "Uncompressing image ${image_xz} failed" ; exit -1;
  fi
  #fi
}

# Main script
download_xz_image
uncompress_xz
