#!/bin/bash
#
# Checkout https://opensource.com/article/20/5/disk-image-raspberry-pi for chroot aspects
#

parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
service_file_path="${parent_path}/observatory.service"
destination_service_file_path="/lib/systemd/system/observatory.service"
requirements_file_path="${parent_path}/requirements.txt"
env_file_path="${parent_path}/.env"


# create user that will be used in subsequent steps
useradd -ms /bin/bash observatory
username=observatory
# Add user to sudoers for reboot/shutdown commands
usermod -aG sudo $username
#username=$(getent passwd 1000 | cut -d: -f1)

# You want to disable the auto-upgrade that can potentially break something ?
sed -i 's/APT::Periodic::Unattended-Upgrade "1";/APT::Periodic::Unattended-Upgrade "0";/g' /etc/apt/apt.conf.d/20auto-upgrades

# Now update and install packages
#apt-get update && apt-get upgrade --yes # This generates plenty of errors related to uboot/kernel stuff
apt-get update
apt-get install --yes\
  build-essential\
  curl\
  graphviz\
  libgraphviz-dev\
  libraspberrypi-bin\
  linux-tools-raspi\
  net-tools

# Install docker
curl -fsSL https://get.docker.com -o get-docker.sh
/bin/bash ./get-docker.sh
rm ./get-docker.sh
usermod -a -G docker ${username}

# Installing docker compose and other tools
apt-get install --yes\
  python3-distutils\
  python3-dev\
  libffi-dev\
  libssl-dev\
  python3\
  python3-pip
# Clean /var/cache/apt/archives/
apt autoremove && apt autoclean
# Install python3 dependencies
pip3 install -r ${requirements_file_path}

#### Custom scripts to support scripts/binary utilities 
# Enable temperature monitoring to access /dev/vchiq for GPU temp
usermod -a -G video $username #This one cannot really work for now
echo 'SUBSYSTEM=="vchiq",GROUP="video",MODE="0660"' > /etc/udev/rules.d/10-vchiq-permissions.rules
chmod a+x ${parent_path}/telegraf/scripts/*.sh
chmod o+x /bin/vcgencmd

# Enable serial access
usermod -a -G dialout $username

# Enable GPIO
groupadd gpio
usermod -a -G gpio $username
echo 'KERNEL=="gpiomem",GROUP="gpio",MODE="0660"' > /etc/udev/rules.d/10-gpiomem-permissions.rules
echo "SUBSYSTEM==\"gpio*\", PROGRAM=\"/bin/sh -c 'find -L /sys/class/gpio/ -maxdepth 2 -exec chown root:gpio {} \; -exec chmod 770 {} \; || true\"" > /etc/udev/rules.d/10-gpio-permissions.rule

# Fix boot problem with FC board
cp ${parent_path}/boot/uboot.env /boot/uboot.env


#### MQTT - TIG stack - Docker compose service definition and startup

# Make sure all path are correct for local config
echo "LOCAL_INSTALL_PATH=${parent_path}" >> ${env_file_path}
# changed default sed separator from / to ~
sed -i "s~LOCAL_USER_NAME~${username}~g" ${service_file_path}
sed -i "s~LOCAL_INSTALL_PATH~${parent_path}~g" ${service_file_path}
cp ${service_file_path} ${destination_service_file_path}
chmod 644 ${destination_service_file_path}
chmod a+x ${parent_path}/influxdb/scripts/init.sh #influxdb specific
# Now configure systemd to load the service file during boot:
systemctl enable observatory.service

# config files for influxdb: enable REST API
# sample taken from https://github.com/influxdata/influxdb/blob/1.8/etc/config.sample.toml
# with some modifications in the [http] section
# Also check the doc for more info: https://docs.influxdata.com/influxdb/v1.8/query_language/manage-database/
#cp $parent_path/influxdb.conf /etc/influxdb/influxdb.conf
#systemctl restart influxdb.service
#curl -XPOST 'http://localhost:8086/query' --data-urlencode 'q=CREATE DATABASE "telegraf"'
#curl -XPOST 'http://localhost:8086/query' --data-urlencode 'q=CREATE USER "telegraf" WITH PASSWORD "Telegr@f"'
#curl -XPOST 'http://localhost:8086/query' --data-urlencode 'q=GRANT ALL ON "telegraf" TO "telegrafuser"'
#curl -XPOST 'http://localhost:8086/query' --data-urlencode 'q=CREATE RETENTION POLICY "52Weeks" ON "telegraf" DURATION 52w REPLICATION 1'

# config files for telegraf: influxdb
#cp $parent_path/telegraf.conf /etc/telegraf/telegraf.conf
#systemctl reload telegraf.service

# config files for telegraf: raspberry Pi dashboard
#usermod -a -G video telegraf
#cp $parent_path/raspberrypi.conf /etc/telegraf/telegraf.d/raspberrypi.conf
#systemctl reload telegraf.service

# Grafana
# Open the following URL in your webbrowser: http://IP_RASPBERRYPI:3000 to reach the login screen.
# The default credentials admin / admin must be changed at first login.
# From the Grafana frontend. To add the first dashboard, mouse over the + just below the Grafana Search at the main page and choose Import. Enter the ID 10578 and Load.

# Final operations, cleanup/anything done after download and file changes are over

# provide access to all those file to user
chown -R ${username}:${username} ${parent_path}

# Sorry but I didn't found any easy and robust way to remap internal docker users ids onto local ${username} id
chmod -R o+rwX ${parent_path}

# Now repair network conf
apt-get install --yes\
  resolvconf
dpkg-reconfigure resolvconf --frontend=noninteractive
