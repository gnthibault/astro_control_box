#!/bin/bash
export LD_LIBRARY_PATH=/hostfs/usr/lib/aarch64-linux-gnu
temp=$(/hostfs/bin/vcgencmd measure_temp)
export LD_LIBRARY_PATH=/usr/lib/aarch64-linux-gnu
temp=$( echo $temp | cut -d'=' -f2 | egrep -o '[0-9]+.[0-9]+')
echo $temp
