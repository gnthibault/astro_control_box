#!/bin/bash

# Enable non-native arch chroot with DNF, adding new binary format information
apt-get install --yes\
  git\
  qemu-system-arm\
  qemu-user-static
systemctl restart systemd-binfmt.service

# Other reference
# https://geoffhudik.com/tech/2020/04/27/scripting-raspberry-pi-image-builds/
