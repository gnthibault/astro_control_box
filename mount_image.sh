#!/bin/bash

# First, image related variables
image_path=./downloads
output_path=./outputs
install_dir=./install_dir
image_iso="$image_path/image.img"
xz_suffix=".tomove"
tmp_image_xz="$image_iso$xz_suffix"
output_image_xz="$output_path/image.img.xz"
tmp_dir=""

# Avoid warning
## sudo update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
## source /etc/default/locale

# https://disconnected.systems/blog/another-bash-strict-mode for
# details.


# Script related utilities
create_temporary_dir() {
    tmp_dir=$(mktemp -d)
    echo "Just created temporary mount dir $tmp_dir"
}

mount_iso_images() {
    # You can start by checking out what is the format of the img
    #fdisk -l $image_iso

    # Increase raw img size by 750MB
    dd if=/dev/zero bs=1M count=750 >> ${image_iso}

    # Create a loop virtual disk
    loop_dir=$(sudo losetup --show -fP "${image_iso}")
    # Increase size of the second partition (the system ext4) to take all available space
    sudo parted --script ${loop_dir} resizepart 2 100%
    sudo partprobe -s ${loop_dir}
    sudo e2fsck -y -f ${loop_dir}p2
    sudo resize2fs -F ${loop_dir}p2

    # Now perform mount
    echo "Mounting image $image_iso partitionsext4 into $loop_dir"
    sudo mount -o loop ${loop_dir}p2 $tmp_dir
    sudo mount -o loop ${loop_dir}p1 $tmp_dir/boot/
}

prepare_chrooted_env_and_launch_install_script() {
    HOST_ARCH=$(dpkg --print-architecture)

    # do stuff to $tmp_dir which is rpi filesystem
    echo "Starting actual image build"
    chroot_dst_install_dir="/var/observatory"
    mnt_dst_install_dir="${tmp_dir}${chroot_dst_install_dir}"
    sudo mkdir -p ${mnt_dst_install_dir}
    sudo cp -r ${install_dir}/* ${mnt_dst_install_dir}

    if [ $HOST_ARCH != "arm64" ]
    then
        prepare_chrooted_env_on_arm64
    else
        prepare_default_chrooted_env
    fi

    # Now running installation script
    echo "Now running installation script"
    sudo chroot $tmp_dir /bin/bash ${chroot_dst_install_dir}/run_pi_install_script.sh
}

prepare_default_chrooted_env() {
    echo "Preparing chroot env on x86: nothing to do for now"
    prepare_chrooted_env_on_arm64
}

prepare_chrooted_env_on_arm64() {
    sudo cp --remove-destination /etc/resolv.conf $tmp_dir/etc/
    echo "Preparing chroot env on arm64: resolvconf/dev/sys/proc mounting"
    #sudo touch $tmp_dir/etc/resolv.conf
    #sudo mount --bind /etc/resolv.conf $tmp_dir/etc/resolv.conf
    #sudo mount --bind /etc/resolv.conf $tmp_dir/run/systemd/resolve/stub-resolv.conf
    sudo mount --bind /dev $tmp_dir/dev
    sudo mount -t proc /proc $tmp_dir/proc
    sudo mount -t sysfs /sys $tmp_dir/sys
}

cleanup_and_umount() {
  # cleanup
  echo "Cleaning up temporary build directory $tmp_dir"
  #sudo umount $tmp_dir/etc/resolv.conf
  #sudo umount $tmp_dir/run/systemd/resolve/stub-resolv.conf
  #if [[ $tmp_dir == /tmp/tmp.* ]]; then
  #  sudo rm $tmp_dir/etc/resolv.conf
  #fi
  #resolvconf issue has been fixed in the installs script
  sudo umount $tmp_dir/dev
  sudo umount $tmp_dir/proc
  sudo umount $tmp_dir/sys
  sudo umount $tmp_dir/boot/
  sudo umount $tmp_dir
  rmdir $tmp_dir
}

generate_output_iso() {
  # build the xz
  if [ ! -f $output_path ]; then
    mkdir -p $output_path
  fi

  echo "Now compressing output image to $output_image_xz"
  xz --compress --threads=4 --keep --suffix=$xz_suffix $image_iso
  mv $tmp_image_xz $output_image_xz
}

# Main script
set -e
trap cleanup_and_umount ERR

#if [[ -n "$1" ]]; then
#    img_name=$1
#else
#    echo "No argument passed, please specify either default or arm64"
#    exit
#fi

# Perform actual work
create_temporary_dir
mount_iso_images
prepare_chrooted_env_and_launch_install_script
cleanup_and_umount
generate_output_iso